const calcular = async (event) => {
	event.preventDefault();
	const form = document.getElementById('frmregistro');
	let jsonData = {};

	for (const pair of new FormData(form)) {
		jsonData[pair[0]] = pair[1];
	}
	const respuesta = await enviarPeticion('Controllers/Primercontroller.php', jsonData);
	crearReglonTabla(respuesta);
}

const crearRenglonVista1 = (datos) => {
	nombreCompleto = datos[1]+ " " + datos[2];
	datos.splice(1, 2, nombreCompleto);
	datos.splice(3, 1);
	tr = document.createElement("tr");
	datos.forEach(element => {
		td = document.createElement("td");
		td.innerText = element;
		tr.appendChild(td);
	});
	document.getElementById("tabla").appendChild(tr);
}

const reemplazarBool = (array) => {
	array.forEach((element, index) => {
		if (element == true){
			array.splice(index, 1, "SI");
		}else if (element == false){
			array.splice(index, 1, "NO");
		}
	  });
	return array;
}

const crearTablaVista2 = async () => {
	respuesta = await enviarPeticion("Controllers/SegundoController.php");
	document.getElementById("tabla");
	Object.entries(respuesta.body).forEach(([key1]) => {
		tr = document.createElement("tr");
		Object.entries(respuesta.body[key1]).forEach(([key, value]) => {
			td = document.createElement("td");
			td.innerText = //voy por aqui
			console.log(key + " " + value);
		});
		
	});
	//console.log(respuesta);
}

const crearTablaVista3 = async (action) => {
	respuesta = await enviarPeticion(action);
	document.createElement("");
}

const enviarPeticion = async (action, jsonBody) => {//
	let direccionDocumento =  document.documentURI;
	let res;

	const URL = `${direccionDocumento.replace(direccionDocumento.slice(direccionDocumento.lastIndexOf("/")+1), '')}Controllers/${action}.php`;
	//const URL = 'Controllers/PrimerController.php'; //por alguna razon funciona asi
	// asdasd
	const headers = new Headers({'Accept': 'application/json','Content-Type': 'application/json'});
	const myInit = {
			method: 'POST',
			headers: headers,
			body: JSON.stringify(jsonBody)
	};
	const request = new Request(URL, myInit);
	try {
		anwswer = await fetch(request);
		anwswer = await anwswer.json();
		res = {body:anwswer};			
	} catch(e) {
		console.log(Error(e.message));
	}
	return res;
}

const recolectarDatos = (tipoDeSalida) => {
	const form = document.getElementById('fomulario');
	if(tipoDeSalida == 'JSON'){
		let datosPersonales = {};
		for (let pair of form){
			if(pair.type == 'checkbox'){
				datosPersonales[pair.name] = pair.checked;
			}else{
				datosPersonales[pair.name] = pair.value;
			}
		}
		return datosPersonales;
	}else { 
		let datosPersonales = new Array();
		let contador = 0;
		for (let pair of form){
			if(pair.nodeName == 'INPUT' || pair.nodeName == 'SELECT'){
				if(pair.type == 'checkbox'){
					datosPersonales[contador] = pair.checked;
				}else{
					datosPersonales[contador] = pair.value;
				}
				contador++;
			}
		}
		return datosPersonales;
	}
}
